
function updateMove( entity, dt )
	-- drag
	if not entity.fixedvel then
	entity.vel[1] = entity.vel[1] * math.pow(game.drag, dt*60)
	entity.vel[2] = entity.vel[2] * math.pow(game.drag, dt*60)

	entity.vel[1] = entity.vel[1] + entity.acc[1] * dt
	entity.vel[2] = entity.vel[2] + entity.acc[2] * dt
	end
	
	entity.pos[1] = entity.pos[1] + entity.vel[1] * dt
	entity.pos[2] = entity.pos[2] + entity.vel[2] * dt
	
	entity.pos = wrapCoord(entity.pos[1], entity.pos[2])

	-- rotation
	entity.angle = entity.angle + entity.rotationSpeed * dt
	if entity.angle < 0 then
		entity.angle = entity.angle + 360
	end
	if entity.angle > 360 then
		entity.angle = entity.angle - 360
	end
end

function mapCoords( entity )
	entity.screenPos = mapToScreen(entity.pos[1], entity.pos[2], entity.layer)
end

function updateEntityBeat( entity, dt )
	entity.pulse.timer = entity.pulse.timer + dt
	if entity.heartRate > 10 and entity.pulse.timer >= 1200 / entity.heartRate then
		entity.pulse.timer = 0
	end

	local x = entity.pulse.timer * 10
	local val = x * math.exp(-x)
	entity.pulse.amount = entity.pulse.min + val * entity.pulse.amp
	
end
