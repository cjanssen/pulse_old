Plankton = {}


function Plankton.init()
	Plankton.list = {}
	Plankton.planktonPics = {
		love.graphics.newImage("img/plankton_500_1.png"),
		love.graphics.newImage("img/plankton_500_2.png"),
		love.graphics.newImage("img/plankton_500_3.png"),
		love.graphics.newImage("img/plankton_500_4.png"),
		love.graphics.newImage("img/plankton_500_5.png"),
		love.graphics.newImage("img/soft_1.png"),
		love.graphics.newImage("img/soft_2.png"),
		love.graphics.newImage("img/soft_3.png"),
		love.graphics.newImage("img/round_1.png"),
		love.graphics.newImage("img/round_2.png"),
		love.graphics.newImage("img/round_3.png"),
		love.graphics.newImage("img/sharp_1.png"),
		love.graphics.newImage("img/sharp_2.png"),
		love.graphics.newImage("img/sharp_3.png")
	}

	Plankton.makeQuadTrees()
	Plankton.updatedCount = 0
	Plankton.updatedList = {}
	Plankton.toDelete = {}

	Plankton.updateCount()
	Plankton.list[1].special = true
end

function Plankton.updateCount()
	if not Plankton.list then
		return
	end

	function setNumberTo(number)
		if table.getn(Plankton.list) > number then
			for i=number+1, table.getn(Plankton.list) do
				Plankton.killPlankton(Plankton.list[i])
			end
		elseif table.getn(Plankton.list) < number then
			local diff = number - table.getn(Plankton.list)
			for i=1,diff do
				Plankton.addOne()
			end
		end
	end

	if Options.density == 1 then
		setNumberTo(100)
	elseif Options.density == 2 then
		setNumberTo(250)
	elseif Options.density == 3 then 
		setNumberTo(450)
	end
end

function Plankton.addOne()
	local i = table.getn(Plankton.list)

	local layerChoice = math.random(3)
	local imgChoice = math.random(table.getn(Plankton.planktonPics))
	if layerChoice == 1 then  -- avoid second half of the table if in foreground
		imgChoice = math.random(8) -- hardcoded 8
	end
	
	local v = {
			number = i,
			pos =  { math.random(game.worldSize[1]), math.random(game.worldSize[2]) },
			screenPos = { -5000, -5000 },
			vel = { math.random()*50 - 25, math.random()*50-25 },
			acc = { 0, 0 },
			imgIndex = imgChoice,
			rotationSpeed = math.random()*60 - 30,
			angle = math.random(360),
			fixedvel = true,
			layer = layerChoice,
			scaleFactor = 1 + (math.random()-0.5),
			special = false
		}
	mapCoords(v)

	table.insert(Plankton.list, v)
	QuadTrees.insert(Plankton.layerQuads[v.layer], v.pos, v)
end

function Plankton.makeQuadTrees()
	local cellSizes = {{200,200}, {400,400}, {800,800}}
	Plankton.layerQuads = {}
	table.insert(Plankton.layerQuads, QuadTrees.init(game.worldSize, cellSizes[1]))
	table.insert(Plankton.layerQuads, QuadTrees.init(game.worldSize, cellSizes[2]))
	table.insert(Plankton.layerQuads, QuadTrees.init(game.worldSize, cellSizes[3]))
	for i,v in ipairs(Plankton.list) do
		QuadTrees.insert(Plankton.layerQuads[v.layer], v.pos, v)
	end
end

function Plankton.update( dt )
	Plankton.removeOld()
	if game.quadPlankton then	
		Plankton.updatedCount = 0
		Plankton.updateInQuads(dt)
		for i = 1,Plankton.updatedCount do
			Plankton.updateQuadCoords(Plankton.updatedList[i])
		end
	else
		for i,v in ipairs(Plankton.list) do
			Plankton.updatePlankton( v, dt )
		end
	end
end

function Plankton.draw()
	if game.quadPlankton then
		for i = 1,Plankton.updatedCount do
			Plankton.drawPlankton(Plankton.updatedList[i])
		end
	else		
		for i,v in ipairs(Plankton.list) do
			Plankton.drawPlankton( v )
		end
	end
end

function Plankton.updateInQuads(dt)
	function myUpdate(plankton)
		Plankton.updatePlankton(plankton, dt)
		Plankton.updatedCount = Plankton.updatedCount + 1
		Plankton.updatedList[Plankton.updatedCount] = plankton
	end

	for layer = 1,3 do
		local sw = (Options.width / VideoModes.scale[1] * 0.5) / game.layerFactors[layer] + 500
		local sh = (Options.height / VideoModes.scale[2] * 0.5)  / game.layerFactors[layer] + 500
		local sc = {game.layerCenters[layer][1] / game.layerFactors[layer], game.layerCenters[layer][2] / game.layerFactors[layer] }

		QuadTrees.eval(Plankton.layerQuads[layer], { sc[1] - sw, sc[2] - sh, sc[1] + sw, sc[2] + sh }, myUpdate )
	end
end


function Plankton.updatePlankton( plankton, dt )
	updateMove( plankton, dt )
	mapCoords( plankton )
end

function insertUnique(t,elem)
	for i,v in ipairs(t) do
		if v==elem then
			return
		end
	end
	table.insert(t,elem)
end

function Plankton.removeOld()
	for i,v in ipairs(Plankton.toDelete) do
			removeOne(Plankton.list, v)
			QuadTrees.removeElement(v.parentQuad,v)
	end
	Plankton.toDelete = {}
end

function Plankton.killPlankton( plankton )
	insertUnique(Plankton.toDelete, plankton)
end

function Plankton.updateQuadCoords( plankton )
	QuadTrees.move(Plankton.layerQuads[plankton.layer], plankton.pos, plankton)
end

function Plankton.drawPlankton( plankton )
	local image = Plankton.planktonPics[plankton.imgIndex]
	local scale = 0.25
	
	local opacity = 1
	love.graphics.setColor(255, 255, 255, 192 * opacity)
	if plankton.layer == 2 then
		scale = 0.5
		love.graphics.setColor(192, 192, 255, 96 * opacity)
	elseif plankton.layer == 3 then
		scale = 1
		love.graphics.setColor(128, 128, 255, 48 * opacity)
	end

	scale = scale * plankton.scaleFactor
	love.graphics.draw(image,
		plankton.screenPos[1], plankton.screenPos[2], plankton.angle * math.pi / 180, scale, scale, image:getWidth()/2, image:getHeight()/2)

	love.graphics.setColor(255, 255, 255, 255)
	
end
