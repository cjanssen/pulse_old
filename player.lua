player = {}

-- todo: there's some weird artifact in the player controls (zoom, windowed, low performance/profiling?)
function player.init()
	player.pos = { 4200, 4200 }
	player.layer = 1
	player.screenPos = { 640, 480 }
	player.vel = { 0, 0 }
	player.acc = { 0, 0 }
	player.af = 4 -- af is acceleration factor
	player.heartRate = 1000
	player.heartPeriod = 1200 / player.heartRate
	player.followers = {}
	player.watchers = {}
	player.attachedCount = 0

	player.pulse = {
		timer = 0,
		min = 0.4,
		amp = 0.2,
		amount = 0.2
	}

	player.rotationSpeed = 0
	player.maxRotationSpeed = 120 -- degrees per second
	player.rotationAcc = 0
	player.angle = 0
	player.hookAngle = 0
	player.hookDist = 95
	player.message = {
		state = 1,
		period = player.heartPeriod,
		beatCount = 0,
		time = 0,
		tolerance = 0.25 * player.heartPeriod,
		silencePeriod = player.heartPeriod * 0.2,
		successPeriod = player.heartPeriod * 0.8
	}
	player.colors = {
		time = 0,
		period = player.message.period + player.message.tolerance,
		current = {120, 0, 255},
		modifier = {120, 0, 255},
		base = {120, 0, 255},

		positive = {120, 48, 255},
		negative = {300, 0, 192},
		affected = {300, 48, 255},
		charge = {120, 64, 255}
    }

	player.playerNote = Sounds.getFriendNote()
	player.secondNote = Sounds.getFriendNote()
	
    player.img = love.graphics.newImage("img/player_cell_4.png")
end

function player.update(dt)
	updateEntityBeat(player, dt)
	player.updateMessage( dt )
	player.updateAffectors( dt )
	player.updateHook( dt )
	player.manageMove( dt )
	updateMove( player, dt )
	mapCoords( player )
end

function player.manageMove( dt )
	-- accelerate in the direction of the mouse
	local mouseX, mouseY

	if game.mode == 4 then
		return
	end

	if game.mode == 2 then
		mouseX = love.mouse.getX() / VideoModes.scale[1]
		mouseY = love.mouse.getY() / VideoModes.scale[2]

	elseif game.mode == 1 then
		mouseX = love.graphics.getWidth() / VideoModes.scale[1] * 0.95
		mouseY = love.graphics.getHeight() / VideoModes.scale[2] * 0.90
	else
		mouseX = love.graphics.getWidth() / VideoModes.scale[1] * 0.5
		mouseY = love.graphics.getHeight() / VideoModes.scale[2] * 0.5
	end

	local dx = mouseX - player.screenPos[1]
	local dy = mouseY - player.screenPos[2]

	player.acc[1] = dx * player.af
	player.acc[2] = dy * player.af

	player.updateRotationSpeed( dt )	
end

function player.updateRotationSpeed( dt )
	local accAngle = getAngle({0, 0}, player.acc)
	local dirAngle = getAngle({0, 0}, player.vel)
	if player.acc[1] == 0 and player.acc[2] == 0 then
		accAngle = dirAngle
	end
	player.rotationAcc = player.rotationAcc * 0.99 + (accAngle - dirAngle) * 2 * dt
	player.rotationSpeed = player.rotationSpeed * 0.99 + player.rotationAcc * dt


	if math.abs(player.rotationSpeed) > player.maxRotationSpeed then
		player.rotationSpeed = math.sign(player.rotationSpeed) * player.maxRotationSpeed
	end
end

function player.updateAffectors( dt )
	if player.colors.time <= 0 then
		player.colors.current = player.colors.base
	else
		player.colors.time = player.colors.time - dt
		if player.message.state == 2 then
			if player.colors.time > player.message.tolerance * 3 then
				player.colors.current = player.colors.base
			elseif player.colors.time > player.message.tolerance * 2 then
				local fraction = (player.colors.time - player.message.tolerance * 2) / (player.message.tolerance )
				player.colors.current = mixColors(player.colors.base, player.colors.modifier, fraction)
			else
			    player.colors.current = player.colors.modifier
			end
		else
			player.colors.current = player.colors.modifier
		end
	end

	local ampDest = 0.2
	local minDest = 0.4
	if player.message.state == 4 and player.colors.time > 0 then
		minDest = 0.2
		ampDest = 0.1
		scaleDest = 0
	elseif player.message.state == 3 and player.colors.time > 0 then
		ampDest = 0.2 + 0.15 * (player.colors.time / player.message.successPeriod)
		minDest = 0.4 + 0.15 * (player.colors.time / player.message.successPeriod)
		scaleDest = 0
	end
	player.pulse.amp = linearApprox(dt, player.pulse.amp, ampDest, 2)
	player.pulse.min = linearApprox(dt, player.pulse.min, minDest, 2)
	
end

function player.updateHook( dt )
	player.hookAngle = player.hookAngle + math.sign(player.rotationSpeed) * 10 * dt
	if player.hookAngle > 360 then
		player.hookAngle = player.hookAngle - 360
	elseif player.hookAngle < 0 then
		player.hookAngle = player.hookAngle + 360
	end
end

function player.draw()
	-- color
	love.graphics.setColor(hsv2rgb(player.colors.current[1], player.colors.current[2], player.colors.current[3], 240 * Titles.characterOpacity))

	love.graphics.draw(player.img, 
		player.screenPos[1], player.screenPos[2], 
		player.angle * math.pi / 180, 
		player.pulse.amount,
	 	player.pulse.amount,
 		player.img:getWidth() * 0.5, player.img:getHeight() * 0.5)
end

function player.mousepressed()
	if game.mode == 4 then
		return
	end

	if Friends.annoyedCount > 0 then
		return
	end

	if player.message.state == 1 then -- first beat
			player.message.state = 2
			player.message.time = 0
			player.message.beatCount = 1	
			player.fire(player.message.beatCount - 1)
	elseif player.message.state == 2 then -- next beats
		if math.abs(player.message.time - player.message.period) < player.message.tolerance then
			player.message.time = player.message.time - player.message.period
			player.message.beatCount = player.message.beatCount + 1
			if player.message.beatCount > table.getn(player.followers) then
				player.message.state = 3
			end
			player.fire(player.message.beatCount - 1)
		else
			player.misfire()
		end
	elseif player.message.state == 3 then -- silence

	elseif player.message.state == 4 then -- forced silence
		if player.message.time > player.message.silencePeriod then
			player.message.state = 1
			player.message.time = 0		
			player.message.beatCount = 0	
		end
	end
end

function player.updateMessage( dt )
	player.message.time = player.message.time + dt

	if player.message.state == 1 then -- first beat
		-- nothing to do
	elseif player.message.state == 2 then -- next beats
		if player.message.time > player.message.period + player.message.tolerance then
			player.misfire()
		end
	elseif player.message.state == 3 then -- silence
		if player.message.time > player.message.period*2 - player.message.tolerance then
			player.message.state = 1
		end
	elseif player.message.state == 4 then -- forced silence
		if player.message.time > player.message.silencePeriod then
			player.message.state = 1
			player.message.time = 0		
			player.message.beatCount = 0	
		end
	end

	if player.pulse.timer == 0 then
		if game.mode == 1 or game.mode == 2 then
			Sounds.playHeartbeat()
		end
	end
end

function player.fire(step)
	if step == 0 or table.getn(player.followers) == 0 then
		Circles.newCircle(player.pos[1], player.pos[2], 1)		
		Friends.affectWatchers(player.playerNote)
		Sounds.playNote(player.playerNote)
	elseif step <= table.getn(player.followers) then
		Friends.fire(player.followers[step])
		Circles.newCircle(player.pos[1], player.pos[2], 2)
		Friends.affectWatchers(player.followers[step].notes.pulse)
	else
		player.misfire()
	end

	if player.message.state == 2 then
		player.colors.modifier = player.colors.charge
		player.colors.period = player.message.period + player.message.tolerance
		player.colors.time = player.colors.period
	elseif player.message.state == 3 then
		Circles.newCircle(player.pos[1], player.pos[2], 1)
		Circles.newCircle(player.pos[1], player.pos[2], 1) 
		Circles.forceLastCircleRadius(50, 400)
		Sounds.playNote(player.secondNote)
		Friends.finalPound()

		player.colors.modifier = player.colors.positive
		player.colors.time = player.message.successPeriod
		
		player.pulse.timer = 0.09
	end
end

function player.misfire()
	player.message.time = 0
	player.message.state = 4

	player.colors.modifier = player.colors.negative
	player.colors.time = player.message.silencePeriod

	Friends.checkMiss()

	Sounds.playNote(Sounds.failNote)
end

