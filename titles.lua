Titles = {}

function Titles.init()
	Titles.titleBanner = love.graphics.newImage("img/title.png")
	Titles.titleOpacity = 1
	Titles.overlayOpacity = 1
	Titles.overlayColor = 0
	Titles.underlayOpacity = 0
	Titles.beginTimer = 0
	Titles.endTimer = 0
	Titles.optionsMsgOpacity = 0
	Titles.optionsMsgTimer = 0

	Titles.helpBanner = love.graphics.newImage("img/help.png")
	Titles.creditsBanner = love.graphics.newImage("img/credits.png")
	Titles.helpOpacity = 0
	Titles.creditsOpacity = 0
	Titles.resetTimer = 0	

	Titles.characterOpacity = 1
	Titles.otherOpacity = 0
	Titles.optionsOpacity = 0
end

function Titles.update( dt )
	if game.mode == 1 then
		Titles.beginTimer = Titles.beginTimer + dt
		if Titles.beginTimer >= 4 then
			Titles.helpOpacity = increaseExponential(dt, Titles.helpOpacity, 0.97)
		end

		Titles.overlayOpacity = decreaseExponential(dt, Titles.overlayOpacity, 0.99)
		Titles.characterOpacity = increaseExponential(dt, Titles.characterOpacity, 0.98)
	end

	if game.mode == 2  then
		Titles.titleOpacity = decreaseExponential(dt, Titles.titleOpacity, 0.95)
		Titles.helpOpacity = decreaseExponential(dt, Titles.helpOpacity, 0.95)
		if Titles.optionsMsgTimer < 6 then
			Titles.optionsMsgTimer = Titles.optionsMsgTimer + dt
			if Titles.optionsMsgTimer > 2 then
				Titles.optionsMsgOpacity = increaseExponential(dt, Titles.optionsMsgOpacity, 0.97)
			end
		else
			Titles.optionsMsgOpacity = decreaseExponential(dt, Titles.optionsMsgOpacity, 0.97)
		end

		Titles.overlayOpacity = decreaseExponential(dt, Titles.overlayOpacity, 0.99)
		Titles.otherOpacity = increaseExponential(dt, Titles.otherOpacity, 0.98)
		Titles.characterOpacity = increaseExponential(dt, Titles.characterOpacity, 0.98)
	end

	if game.mode == 1 and love.mouse.isDown("l") then
		game.setMode(2)
	end

	if game.mode == 2 and player.attachedCount >= game.friendgoal then
		game.setMode(3)
	end

	if game.mode == 3 then
		Titles.overlayColor = 1
		Titles.titleOpacity = decreaseExponential(dt, Titles.titleOpacity, 0.95)
		Titles.endTimer = Titles.endTimer + dt
		if Titles.endTimer < 4 then
			Titles.overlayOpacity = increaseExponential(dt, Titles.overlayOpacity, 0.97)
		else
			Titles.overlayOpacity = decreaseExponential(dt, Titles.overlayOpacity, 0.99)
			Titles.underlayOpacity = 1
			if Titles.resetTimer == 0 then				
				Titles.creditsOpacity = 1
				if love.mouse.isDown("l") then
					Titles.resetTimer = dt
				end
			else
				Titles.creditsOpacity = decreaseExponential(dt, Titles.creditsOpacity, 0.95)
				Titles.resetTimer = Titles.resetTimer + dt
				if Titles.resetTimer >= 1.2 then
					game.restart = true 
				end
			end
		end
		Titles.fadeout(dt)
	end
end

function Titles.fadeout( dt )
	love.audio.setVolume( love.audio.getVolume() * 0.99 )
end

function Titles.draw()
	if Titles.underlayOpacity > 0 then
		love.graphics.setColor(0, 0, 0)
		love.graphics.rectangle("fill",0, 0, love.graphics.getWidth()/VideoModes.scale[1], love.graphics.getHeight()/VideoModes.scale[2])
	end

	if Titles.titleOpacity > 0.001 then
		love.graphics.setColor(255, 255, 255, 255 * Titles.titleOpacity * Titles.characterOpacity)
		love.graphics.draw(Titles.titleBanner, 70/game.zoom, (love.graphics.getHeight()/VideoModes.zoom[2] - 200)/game.zoom, 0, 0.5 / game.zoom, 0.5 / game.zoom)
	end

	if Titles.helpOpacity > 0.001 then
		love.graphics.setColor(255, 255, 255, 255 * Titles.helpOpacity * Titles.characterOpacity)
		love.graphics.draw(Titles.helpBanner,
			love.graphics.getWidth()/VideoModes.scale[1]/2 - Titles.helpBanner:getWidth() * 0.35 / game.zoom,
		 	love.graphics.getHeight()/VideoModes.scale[2]/2 - Titles.helpBanner:getHeight() * 0.5 / game.zoom,
			0, 0.7 / game.zoom, 0.7 / game.zoom)
	end

	if Titles.optionsMsgOpacity > 0.001 then
		love.graphics.setColor(255, 255, 255, 255 * Titles.optionsMsgOpacity * Titles.characterOpacity)
		love.graphics.print("F1 - Options", love.graphics.getWidth()/VideoModes.scale[1] - 150, 20)
	end

	if Titles.creditsOpacity > 0.001 then
		love.graphics.setColor(255, 255, 255, 255 * Titles.creditsOpacity)
		love.graphics.draw(Titles.creditsBanner,
			love.graphics.getWidth()/VideoModes.scale[1]/2 - Titles.creditsBanner:getWidth() * 0.35 / game.zoom,
		 	love.graphics.getHeight()/VideoModes.scale[2]/2 - Titles.creditsBanner:getHeight() * 0.4 / game.zoom,
			0, 0.7 / game.zoom, 0.7 / game.zoom)
	end

	if Titles.overlayOpacity > 0.001 then
		love.graphics.setColor(255 * Titles.overlayColor, 255 * Titles.overlayColor, 255 * Titles.overlayColor, 255 * Titles.overlayOpacity)
		love.graphics.rectangle("fill",0,0,love.graphics.getWidth()/VideoModes.scale[1], love.graphics.getHeight()/VideoModes.scale[2])
	end
end
