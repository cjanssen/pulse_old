game = {}

function game.load()
	VideoModes.init()
	game.initfont()
	Options.load()
	Options.evalFailsafe()
	Options.save()
	VideoModes.reloadScreen()
	Sounds.init()
	Titles.init()
	game.initvalues()
	player.init()
	Plankton.init()
	Circles.init()
	Friends.init()
	game.centerInPlayer()
	Sounds.start()
end

function game.quitting()
	Options.resetFailsafe()
end

-- game modes
-- 1 . title
-- 2 . ingame
-- 3 . endtitle
-- 4 . options
function game.initvalues()
	game.friendgoal = 8
	game.restart = false
	game.setMode(1)
	local baseSize = 15000
	game.worldSize = { baseSize, baseSize * 3 * 0.5 / math.sqrt(3) }
	game.layerFactors = { 1, 0.5, 0.25 }
	game.drag = 0.96
	love.mouse.setVisible(false)
	game.layerCenters = { { 640, 480 }, { 640, 480 }, { 640, 480 } }
	game.screenVel = 300
	game.fps = 0

	game.updateScaling(0, true)
	game.quadPlankton = true
end

function game.testingValues()
	game.specialPlankton = true
	game.hasFriends = true
	game.showFPS = false
end

function game.initfont()
	game.font = love.graphics.newFont("fnt/Mathlete-Bulky.otf", 42)
	love.graphics.setFont(game.font)
end

function game.update(dt)
	if dt > 0.5 then return end -- avoid jumps below 2fps
	game.updateScaling(dt)
	Titles.update(dt)
	Options.update(dt)	
	player.update(dt)
	Plankton.update(dt)
	if game.mode ~= 1 then
		Circles.update(dt)
		Friends.update(dt)
	end
	game.screenupdate(dt)

	if game.restart then
		Options.resetFailsafe()
		game.load()
	end

	if game.showFPS then
		if dt > 0 then
			game.fps = 0.9 * game.fps + 0.1 / dt
		else
			game.fps = 0.9 * game.fps
		end
	end
end

function game.screenupdate(dt)
	game.parallax(dt, game.layerCenters[1], 1)
	game.layerCenters[2][1] = game.layerCenters[1][1]/2
	game.layerCenters[2][2] = game.layerCenters[1][2]/2
	game.layerCenters[3][1] = game.layerCenters[1][1]/4
	game.layerCenters[3][2] = game.layerCenters[1][2]/4
end

function game.parallax(dt, layer, factor)
	local dx = player.pos[1] - layer[1]
	local dy = player.pos[2] - layer[2]

	-- wrap around
	if dx > game.worldSize[1] / 2 then
		layer[1] = layer[1] + game.worldSize[1]
	elseif dx < -game.worldSize[1]/2 then
		layer[1] = layer[1] - game.worldSize[1]
	end
	if dy > game.worldSize[2] / 2 then
		layer[2] = layer[2] + game.worldSize[2]
	elseif dy < -game.worldSize[2]/2 then
		layer[2] = layer[2] - game.worldSize[2]
	end

	dx = player.pos[1] - layer[1]
	dy = player.pos[2] - layer[2]

	local dist = math.sqrt(dx*dx + dy*dy)
	if dist > game.screenVel*factor*dt then
		dx = dx / dist * game.screenVel*factor*dt
		dy = dy / dist * game.screenVel*factor*dt
	end
	layer[1] = layer[1] + dx
	layer[2] = layer[2] + dy
end

function game.centerInPlayer()
	game.layerCenters[1][1] = player.pos[1]
	game.layerCenters[1][2] = player.pos[2]
	game.layerCenters[2][1] = game.layerCenters[1][1]
	game.layerCenters[2][2] = game.layerCenters[1][2]
	game.layerCenters[3][1] = game.layerCenters[1][1]
	game.layerCenters[3][2] = game.layerCenters[1][2]
end

function game.updateScaling(dt, insta)
	local zoom = 0.5 + 0.75 * Options.zoom
	if insta then
		game.zoom = zoom
	else	
		game.zoom = linearApprox(dt, game.zoom, zoom, 0.5)
	end
	VideoModes.scale = {VideoModes.zoom[1] * game.zoom, VideoModes.zoom[2] * game.zoom}
	love.graphics.setLineWidth(math.max(1, 1/VideoModes.scale[1], 1/VideoModes.scale[2]))
	game.screenWidth = Options.width/VideoModes.scale[1]
	game.screenHeight = Options.height/VideoModes.scale[2]
end

function game.draw()
	love.graphics.push()
	love.graphics.scale(VideoModes.scale[1], VideoModes.scale[2])
	love.graphics.setColor(128,192,255,32)
	love.graphics.rectangle("fill",0,0,love.graphics.getWidth() / VideoModes.scale[1], love.graphics.getHeight()/VideoModes.scale[2])
	love.graphics.setColor(255,255,255)
	
	Plankton.draw()
	if game.mode ~= 1 then
		Circles.draw()
		Friends.draw()
	end

	player.draw()

	if game.showFPS then
		love.graphics.setColor(255,255,255)
		local yy = 20
		if game.showPlayerCoords then
			yy = yy + 20
		end
		love.graphics.print(math.floor(game.fps),yy,0)
	end
	
	Options.draw()
	Titles.draw()

	love.graphics.pop()
end

function game.updatePlanktonCount()
	Plankton.updateCount()
end

function game.setMode(newMode)
	if game.mode == 1 and newMode == 2 then
		Friends.avoidPlayer()
	elseif newMode == 3 then
		Sounds.tempMuteSfx()
	end
	game.mode = newMode
end


function game.keypressed( key )
	if key == "escape" then
		if game.mode == 4 then
			Options.toggle()
		else
			love.event.push("quit")
			return
		end
	end

	if key == "f" then
		Options.fullscreen = 1-Options.fullscreen
		VideoModes.reloadScreen()
	end

	if key == "f1" then
		Options.toggle()
	end
end

function game.mousepressed(x, y, button)
	if game.mode == 2 then
		player.mousepressed()
	end
end


