Friends = {}


function Friends.init()

	Friends.accVel = 50
	Friends.distLimitSq = 500 * 500
	Friends.friction = 0.95
	Friends.overshoot = 3000

	Friends.colors =  {
		base = {300,32,255},
		leaving = {240,32,255},
		annoyed = {359, 32, 255}				
	}

	Friends.triangleimage = love.graphics.newImage("img/triangle.png")

	Friends.list = {}
	
	-- states
	-- 1. wandering
	-- 2. close
	-- 3. engaged
	-- 4. escaping

	for i = 1,24 do
		local p = { math.random(game.worldSize[1]), math.random(game.worldSize[2]) }
		local st = 5 --math.random(10) + 5

		table.insert(Friends.list, {
			pos = { p[1], p[2] },
			screenPos = { p[1], p[2] },
			vel = { 0, 0 },
			acc = { 0, 0 },
			imgIndex = math.random(3),
			layer = 1,

			state = 1,
			dest = {0,0},
			oldstate = 0,
			resetdest = true,

			--heartRate = 500 + math.random(1500),
			heartRate = player.heartRate,
			strength = st,
			convinced = 0,
			poundCount = 0,
			annoyedTimer = 0,
			isAnnoyed = false,
			mistrust = 0,

			pulse = {
				timer = 0,
				min = 0.2 + st/40,
				amp = 0.15,
				amount = 0.35 
			},

			rotationSpeed = (math.random()*45 + 15) * math.sign(math.random()-0.5),
			angle = math.random(360),
			
			core = {
				index = math.random(3),
				offset = { math.random(60) - 30, math.random(60) - 30 },
				rotationSpeed = math.random() * 180 - 90,
				angle = math.random(360),
				size = math.random() * 0.3 + 0.25,
			},

			slime = {
				index = math.random(4),
				rotationSpeed = math.random() * 90 - 45,
				angle = math.random(360),
				size = math.random() * 0.4 + 0.4,
			},

			message = {
				idleT = 0
			},

			hook = {
				hooked = false,
				angle = 0,
				currentAngle = 0,
				distToPlayerSq = 1000,
				goalScale = 1,
				currentScale = 1,
				slimeScale = 1,
				rotationDamp = math.random() * 0.35 + 0.4,
				radiusAmp = 0,
				radiusTime = 0,
				radiusSpeed = 50 + math.random() * 80,
			},

			colors = {
				goal  = Friends.colors.base,
				current = Friends.colors.base,
				convinced = false, -- will be overwritten
			},

			notes = {
				pulse = false,
				angry = false,
				beat = false,
			},

			arrow = {
				opacity = 0,	
				screenPos = {0,0},
				angle = 0,
			},

			echo = {
				on = false,
				timer = 0,
				note = false,
			}
		} )

	end

	if not game.hasFriends then
		Friends.list = {}
	end

	Friends.annoyedCount = 0
	Friends.poundLimit = 3
	Friends.missLimit = 3

	Friends.imgs = {
		love.graphics.newImage("img/big_cell_core_1.png"),
		love.graphics.newImage("img/big_cell_core_2.png"),
		love.graphics.newImage("img/big_cell_core_3.png") 
	}

	Friends.coreimgs = {
		love.graphics.newImage("img/core_1.png"),
		love.graphics.newImage("img/core_2.png"),
		love.graphics.newImage("img/core_3.png")
	}

	Friends.slimeimgs = {
		love.graphics.newImage("img/big_cell_slime_1.png"),
		love.graphics.newImage("img/big_cell_slime_2.png"),
		love.graphics.newImage("img/big_cell_slime_3.png"),
		love.graphics.newImage("img/big_cell_slime_4.png")
	}

	Friends.colorPool = {}

end

function Friends.avoidPlayer()
	for i,v in ipairs(Friends.list) do
		if distWrapSq(player.pos, v.pos) < game.worldSize[1]*game.worldSize[2]/25 then
			v.pos[1] = v.pos[1] + game.worldSize[1]/2
			v.pos[2] = v.pos[2] + game.worldSize[2]/2
		end
	end
end

function Friends.update( dt )
	for i,v in ipairs(Friends.list) do
		Friends.updateFriend( v, dt )
	end
end

function Friends.draw()
	for i,v in ipairs(Friends.list) do
		Friends.drawFriend( v )
	end
end

function Friends.playOwnNote(friend)
	if friend.notes.pulse == false then
		friend.notes.pulse = Sounds.getFriendNote()
	end
	Sounds.playNote(friend.notes.pulse)
end

function Friends.playAngryNote(friend)
	if friend.notes.angry == false then
		friend.notes.angry = Sounds.getAngryNote()
	end
	Sounds.playNote(friend.notes.angry)
end

function Friends.playBeatNote(friend)
	if friend.notes.beat == false then
		friend.notes.beat = Sounds.getBeatNote()
	end
	Sounds.playNote(friend.notes.beat)
end

function Friends.perturbate( friend )
	friend.acc = { friend.acc[1] + (math.random()-0.5) * Friends.accVel, 
					friend.acc[2] + (math.random()-0.5) * Friends.accVel }
		
end


function Friends.slowDown(friend, friction, dt)
	local factor = math.pow(friction, dt * 60)
	if factor <= 1  then
		friend.acc[1] = friend.acc[1] * factor
		friend.acc[2] = friend.acc[2] * factor
	end
end

function Friends.updateFriend( friend, dt )
		if friend.state == 1 then -- wander
		
			Friends.slowDown(friend, 0.999, dt)
			Friends.perturbate(friend)
			local distSq = distWrapSq(player.pos, friend.pos)
			friend.message.idleT = 1200/friend.heartRate
			if distSq < Friends.distLimitSq then
				friend.state = 2
				Friends.watch(friend)
			end

		elseif friend.state == 2 then -- close
			if game.mode ~= 4 then
				Friends.slowDown(friend, Friends.friction, dt)
				local distSq = distWrapSq(player.pos, friend.pos)
				if distSq > Friends.distLimitSq * 1.44 then
					friend.state = 1
					Friends.unwatch(friend)
				end


				if friend.echo.on then
					friend.echo.timer = friend.echo.timer - dt
					if friend.echo.timer <= 0 then
						friend.echo.on = false
						Friends.doEcho(friend)
					end
				else

					friend.message.idleT = friend.message.idleT - dt
					if friend.message.idleT <= 0 then
						Friends.resetBeatTimer(friend)
						if player.message.state == 1 or player.message.state == 3 then
							Friends.doBeat(friend)
						end
					end
				end
			end
		elseif friend.state == 3 then -- engaged
			if friend.oldstate ~= 3 then
				friend.hook.hooked = false
			else
				friend.hook.distToPlayerSq = distWrapSq(player.pos, friend.pos)
				if friend.hook.distToPlayerSq < player.hookDist*player.hookDist then
					if not friend.hook.hooked then
						player.attachedCount = player.attachedCount + 1
						friend.hook.radiusTime = -90
					end
					friend.hook.hooked = true
				end
			end

			Friends.updateHookScale( friend, dt )

			if not friend.hook.hooked then
				local accCap = 1400
				if distWrapSq(player.pos, friend.pos) > Friends.distLimitSq then
					accCap = Friends.distLimitSq
				end


				friend.dest = player.pos

				-- wrap
				if friend.dest[1] > friend.pos[1] + game.worldSize[1]*0.5 then
					friend.dest[1] = friend.dest[1] - game.worldSize[1]
				elseif friend.dest[1] < friend.pos[1] - game.worldSize[1]*0.5 then
					friend.dest[1] = friend.dest[1] + game.worldSize[1]
				end

				if friend.dest[2] > friend.pos[2] + game.worldSize[2]*0.5 then
					friend.dest[2] = friend.dest[2] - game.worldSize[2]
				elseif friend.dest[2] < friend.pos[2] - game.worldSize[2]*0.5 then
					friend.dest[2] = friend.dest[2] + game.worldSize[2]
				end
			
				friend.acc[1] = (friend.dest[1] - friend.pos[1])*2
				if math.abs(friend.acc[1]) > accCap then
					friend.acc[1] = math.sign(friend.acc[1]) * accCap
				end

				friend.acc[2] = (friend.dest[2] - friend.pos[2])*2
				if math.abs(friend.acc[2]) > accCap then
					friend.acc[2] = math.sign(friend.acc[2]) * accCap
				end
			else
				local angle = math.mod(friend.hook.angle + player.hookAngle + player.angle, 360)

				if angleDiff(angle, friend.hook.currentAngle) > math.max(dt*100,2) then
					friend.hook.currentAngle = friend.hook.currentAngle + player.rotationSpeed * friend.hook.rotationDamp * dt
					angle = friend.hook.currentAngle
				end
				local radius = player.hookDist * (0.75 + 0.25 * friend.hook.radiusAmp)
				friend.pos = {player.pos[1] + math.cos(angle*math.pi/180) * radius, 
							   player.pos[2] + math.sin(angle*math.pi/180) * radius}
				friend.acc = {0,0}
			end
			friend.hook.currentAngle = getAngle(player.pos, friend.pos)

			-- match player beat
			friend.pulse.timer = player.pulse.timer

		elseif friend.state == 4 then -- escaping
			friend.acc[1] = friend.pos[1] - player.pos[1]
			friend.acc[1] = math.sign(friend.acc[1]) * (math.sqrt(Friends.distLimitSq*9) - math.abs(friend.acc[1])) * 0.5
			friend.acc[2] = friend.pos[2] - player.pos[2]
			friend.acc[2] = math.sign(friend.acc[2]) * (math.sqrt(Friends.distLimitSq*9) - math.abs(friend.acc[2])) * 0.5

			Friends.updateHookScale( friend, dt )

			if friend.hook.hooked then
				friend.hook.hooked = false
				player.attachedCount = player.attachedCount - 1
			end
			
			local distSq = distWrapSq(player.pos, friend.pos)
			if distSq > Friends.distLimitSq * 9 then
				friend.state = 1
			end
		elseif friend.state == 5 then -- annoyed
			friend.annoyedTimer = friend.annoyedTimer + dt
			if friend.annoyedTimer >= 1200/friend.heartRate and friend.isAnnoyed then
				Friends.respond(friend)
			elseif friend.annoyedTimer > 2400/friend.heartRate then
					friend.isAnnoyed = false
					Friends.annoyedCount = Friends.annoyedCount - 1
				if table.getn(player.followers) == 0 then
					Friends.unwatch(friend)
					Friends.leave(friend)
				else
					friend.state = 2
				end
			end
		end

		-- store old state
		friend.oldstate = friend.state

	-- regular updates
	updateEntityBeat(friend, dt)
	Friends.updateColors( friend, dt )
	Friends.updateCore( friend, dt )
	updateMove( friend, dt )
	mapCoords( friend )	
	Friends.updateArrow( friend, dt )
end

function Friends.finalPound()
	if table.getn(player.watchers) > 0 then
		Friends.join(player.watchers[1])
		Friends.unwatch(player.watchers[1])
	end
end

function Friends.affectWatchers(note)
	if table.getn(player.watchers) == 0 then
		return
	end
	-- change color
	player.watchers[1].convinced = player.watchers[1].convinced + 1
	Friends.startEcho(player.watchers[1], note)
end


function Friends.fire(friend)
	Circles.newCircle(friend.pos[1], friend.pos[2], 1)
	Friends.playOwnNote(friend)
end

function Friends.startEcho(friend, note)
	if not friend.echo.on then
		if friend.heartRate > 100 then
			friend.echo.timer = 600 / friend.heartRate
		else
			friend.echo.timer = 6
		end
		friend.echo.on = true
		friend.echo.note = note
	end
end

function Friends.doEcho(friend)
	Circles.newCircle( friend.pos[1], friend.pos[2], 2 )
	Sounds.playNoteVolume(friend.echo.note, 0.45)
end

function Friends.doBeat(friend)
	if friend.poundCount == Friends.poundLimit then
		Friends.annoy(friend)
	else
		Circles.newCircle( friend.pos[1], friend.pos[2], 3 )
		Friends.playBeatNote(friend)
		friend.poundCount = friend.poundCount + 1
	end
end

function Friends.resetBeatTimer(friend)
	if friend.heartRate > 100 then
		friend.message.idleT = 2400 / friend.heartRate
	else
		friend.message.idleT = 24
	end
end

function Friends.annoy(friend)
	friend.poundCount = 0
	friend.annoyedTimer = 0	
	Friends.resetBeatTimer(friend)

	-- continue pounding if player has no followers
	if table.getn(player.followers) > 0 then	
		friend.state = 5
		Friends.annoyedCount = Friends.annoyedCount + 1
		friend.isAnnoyed = true
	end
end

function Friends.respond(friend)
	friend.isAnnoyed = false
	Circles.newCircle(friend.pos[1], friend.pos[2], 4)
	Friends.playAngryNote(friend)

	-- affect first follower
	if table.getn(player.followers) > 0 then
		player.followers[1].mistrust = player.followers[1].mistrust + 1
		if player.followers[1].mistrust == Friends.missLimit then
			Friends.leave(player.followers[1])
		end
	end
end

function Friends.join(friend)
	friend.heartRate = player.heartRate
	friend.state = 3
	table.insert(player.followers, friend)
	Friends.recomputeHooks()
	for i,f in ipairs(player.followers) do
		Friends.playOwnNote(f)
	end
end

function Friends.recomputeHooks()
	local n = table.getn(player.followers)
	for i,v in ipairs(player.followers) do
		v.hook.angle = 360 * i / n
	end
end

function Friends.updateHookScale( friend, dt )
	if friend.state == 3 then
		friend.hook.goalScale = 0.70 + 0.30 * math.sqrt(friend.hook.distToPlayerSq / Friends.distLimitSq)
		if friend.hook.goalScale > 1 then
			friend.hook.goalScale = 1
		end
	else
		friend.hook.goalScale = 1
	end

	local diff = friend.hook.goalScale - friend.hook.currentScale
	local inc = math.sign(diff) * 0.5
	if math.abs(inc) > math.abs(diff) then
		inc = diff
	end

	friend.hook.currentScale = friend.hook.currentScale +  inc * dt
	friend.hook.slimeScale = friend.hook.currentScale * friend.hook.currentScale

	-- hook radius
	friend.hook.radiusTime = (friend.hook.radiusTime + friend.hook.radiusSpeed * dt) % 360
	friend.hook.radiusAmp = 1 - math.pow(((math.sin(friend.hook.radiusTime * math.pi / 180)+1)*0.5),3)
end

function Friends.leave(friend)
	friend.state = 4
	local ndx = 0
	for i=1,table.getn(player.followers) do
		if player.followers[i] == friend then
			ndx = i
		end
	end
	if ndx ~= 0 then
		table.remove(player.followers, ndx)
	end
	Friends.recomputeHooks()
	friend.message.available = false
	Sounds.playNote(Sounds.leaveNote)
end

function Friends.watch(friend)
	table.insert(player.watchers, friend)
	friend.poundCount = 0
end

function Friends.unwatch(friend)
	local ndx = 0
	for i=1,table.getn(player.watchers) do
		if player.watchers[i] == friend then
			ndx = i
		end
	end
	if ndx ~= 0 then
		player.watchers.convinced = 0
		friend.poundCount = 0
		table.remove(player.watchers, ndx)
	end

	if table.getn(player.watchers) == 0 then
		for i,v in ipairs(player.followers) do
			v.mistrust = 0
		end
	end
end

function Friends.checkMiss()
	-- watchers get annoyed
	for i,v in ipairs(player.watchers) do
		v.convinced = 0
		Friends.annoy(v)
	end
end

function Friends.updateCore( friend, dt )
	friend.core.angle = friend.core.angle + friend.core.rotationSpeed * dt
	if  friend.core.angle > 360 then
		friend.core.angle = friend.core.angle - 360
	elseif friend.core.angle < 0 then
		friend.core.angle = friend.core.angle + 360
	end

	friend.slime.angle = friend.slime.angle + friend.slime.rotationSpeed * dt
	if  friend.slime.angle > 360 then
		friend.slime.angle = friend.slime.angle - 360
	elseif friend.slime.angle < 0 then
		friend.slime.angle = friend.slime.angle + 360
	end
end

function Friends.updateArrow( friend, dt )
	-- the opacity is a gradient:
	-- if the friend is in the screen, the value is 0
	-- it raises with the distance to a close border
	-- then it fades slowly with the distance

	local border = 12
	local sc = { Options.width / VideoModes.scale[1], Options.height / VideoModes.scale[2] }
	local screendist = math.sqrt(sc[1]*sc[1]+sc[2]*sc[2]) * 0.5
	local ringdist = screendist * 2
	local longdist = screendist * 5
	local dx = friend.screenPos[1] - sc[1] * 0.5
	local dy = friend.screenPos[2] - sc[2] * 0.5
	local distSq = dx * dx + dy * dy

	-- opacity of the arrow
	if distSq < screendist*screendist then
		friend.arrow.opacity = 0
	elseif distSq < ringdist*ringdist then
		friend.arrow.opacity = 1 - (ringdist - math.sqrt(distSq)) / (ringdist - screendist)
	else
		friend.arrow.opacity = (longdist - math.sqrt(distSq)) / (longdist - ringdist)
	end

	if friend.arrow.opacity < 0 then
		friend.arrow.opacity = 0
	end

	if friend.arrow.opacity > 0 then
		-- angle of the arrow
		friend.arrow.angle = math.atan2(dy, dx)

		-- position of the arrow
		local anglelimit = math.atan2(sc[2],sc[1])
		if friend.arrow.angle <= anglelimit and friend.arrow.angle > -anglelimit then
			-- first quadrant (right)
			friend.arrow.screenPos = {sc[1] - border, sc[2] * 0.5 + sc[1] * 0.5 * math.tan(friend.arrow.angle) }
			friend.arrow.screenPos[2] = math.max(border, math.min( friend.arrow.screenPos[2], sc[2]-border ) )
		elseif friend.arrow.angle > anglelimit and friend.arrow.angle <= math.pi - anglelimit then
			-- second quadrant (down)
			friend.arrow.screenPos = {sc[1] * 0.5 + sc[2] * 0.5 / math.tan(friend.arrow.angle), sc[2] - border }
			friend.arrow.screenPos[1] = math.max(border, math.min( friend.arrow.screenPos[1], sc[1]-border ) )
		elseif friend.arrow.angle <= -anglelimit and friend.arrow.angle > -math.pi + anglelimit then
			-- fourth quadrant (up)
			friend.arrow.screenPos = {sc[1] * 0.5 - sc[2] * 0.5 / math.tan(friend.arrow.angle), border }
			friend.arrow.screenPos[1] = math.max(border, math.min( friend.arrow.screenPos[1], sc[1]-border ) )			
		else
			-- third quadrant (left)
			friend.arrow.screenPos = {border, sc[2] * 0.5 - sc[1] * 0.5 * math.tan(friend.arrow.angle)}
			friend.arrow.screenPos[2] = math.max(border, math.min( friend.arrow.screenPos[2], sc[2]-border ) )
		end
	end
end

function Friends.getConvincedColor( friend )
	if friend.colors.convinced == false then	
		if table.getn(Friends.colorPool) == 0 then
			-- generate colorpool
			local sortList = {}
			for i=1,14 do
				table.insert(sortList,(i-7)*15 + 120)
			end
			while table.getn(sortList) > 0 do
				local index = math.random(table.getn(sortList))
				table.insert(Friends.colorPool, sortList[index])
				table.remove(sortList, index)
			end
		end

		friend.colors.convinced = { Friends.colorPool[1], 32, 255 }
		table.remove(Friends.colorPool, 1)
	end

	return friend.colors.convinced
end

function Friends.updateColors( friend, dt )
		if friend.state == 1 then -- floating
		friend.colors.current = Friends.colors.base
	elseif friend.state == 2 then -- around
		if friend.convinced > 0 then
			local numiters = table.getn(player.followers) + 2
			local fraction = 32 * friend.convinced / numiters
			local h,s,v,a = rgb2hsv(255-fraction, 223+fraction, 255-fraction, 255)
			friend.colors.current = {h,s,v}
		else
			friend.colors.current = Friends.colors.base
		end
	elseif friend.state == 3 then -- following
		friend.colors.current = Friends.getConvincedColor(friend) --friend.colors.convinced
	elseif friend.state == 4 then -- leaving
		friend.colors.current = Friends.colors.leaving
	elseif friend.state == 5 then -- annoyed
		friend.colors.current = Friends.colors.annoyed
	end

end

function Friends.drawFriend( friend )
	-- color
	love.graphics.setColor(hsv2rgb(friend.colors.current[1], friend.colors.current[2], friend.colors.current[3], 255*Titles.otherOpacity))

	-- slime
	local slimeimage = Friends.slimeimgs[friend.slime.index]
	love.graphics.draw(slimeimage, 
					friend.screenPos[1],
					friend.screenPos[2],
					(friend.angle + friend.slime.angle) * math.pi / 180,
					friend.slime.size * friend.hook.slimeScale,
					friend.slime.size * friend.hook.slimeScale,
					slimeimage:getWidth() * 0.5 - friend.core.offset[1],
					slimeimage:getHeight() * 0.5 - friend.core.offset[2])
	

	-- center
	local image = Friends.imgs[friend.imgIndex]
	local ofsx, ofsy = 0,0
	local scalex, scaley = 1,1
	if friend.state == 5 then -- trembling when annoyed
		ofsx = math.random() * 6 - 3
		ofsy = math.random() * 6 - 3
		scalex = (math.random()-0.5)*0.3 + 0.8
		scaley = (math.random()-0.5)*0.3 + 0.8
	end
	scalex = scalex * friend.pulse.amount * friend.hook.currentScale
	scaley = scaley * friend.pulse.amount * friend.hook.currentScale
	
	love.graphics.draw(image, 
				friend.screenPos[1] + ofsx, 
				friend.screenPos[2] + ofsy, 
				friend.angle* math.pi / 180, 
				scalex, 
				scaley, 
				image:getWidth() * 0.5, 
				image:getHeight() * 0.5)

	-- core
	local coreimage = Friends.coreimgs[friend.core.index]

	local cos = math.cos(friend.angle * math.pi / 180)
	local sin = math.sin(friend.angle * math.pi / 180)
	ofsx = friend.core.offset[1] * cos - friend.core.offset[2] * sin
	ofsy = friend.core.offset[2] * cos + friend.core.offset[1] * sin
	
	local coreScale = ((friend.pulse.amount - friend.pulse.min) * 2 + friend.pulse.min * friend.core.size) * friend.hook.currentScale

	if friend.state == 5 then -- trembling when annoyed
		ofsx = ofsx + math.random() * 6 - 3
		ofsy = ofsy + math.random() * 6 - 3
	end

	love.graphics.draw(coreimage, 
					friend.screenPos[1] + ofsx * scalex,
					friend.screenPos[2] + ofsy * scaley,
					(friend.angle + friend.core.angle) * math.pi / 180,
					coreScale,
					coreScale,
					coreimage:getWidth() * 0.5 - friend.core.offset[1] * scalex,
					coreimage:getHeight() * 0.5 - friend.core.offset[2] * scaley)

	-- triangle
	if friend.arrow.opacity > 0 then
		love.graphics.setColor(255,255,255, 255 * Titles.otherOpacity * friend.arrow.opacity)
		love.graphics.draw(Friends.triangleimage, 
					  	friend.arrow.screenPos[1], 
						friend.arrow.screenPos[2],
						friend.arrow.angle, -- this one in radians
						0.5, 0.5,
						Friends.triangleimage:getWidth() * 0.5,
						Friends.triangleimage:getHeight() * 0.5)
--		love.graphics.print(friend.arrow.angle, 10, 0)
	end
	love.graphics.setColor(255,255,255)
end
