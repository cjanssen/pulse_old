-- naive implementation
QuadTrees = {}
QuadTrees.totalCount = 1

function QuadTrees.init(screenSize)
	QuadTrees.dimensions = { screenSize[1], screenSize[2] }
    QuadTrees.itemLimit = 6
	return QuadTrees.newQuad( {0,0,screenSize[1],screenSize[2]}, nil )
end

function QuadTrees.insert(quad,point,element)  
  if quad.childrenCount == 0 and table.getn(quad.elements) < QuadTrees.itemLimit then
    table.insert(quad.elements, element)
    element.parentQuad = quad
    element.pos = {point[1], point[2]}
  else
    -- split
    for i,v in ipairs(quad.elements) do
      QuadTrees.createChildAndInsert(quad, v.pos, v)
    end
    quad.elements = {}
    QuadTrees.createChildAndInsert(quad, point, element)
    
  end
end


function QuadTrees.move(quad, newPosition, element)
    if not QuadTrees.pointInBox(newPosition, element.parentQuad) then
        local formerParent = element.parentQuad
        QuadTrees.insert(quad, newPosition, element)
        QuadTrees.removeElement(formerParent, element)
    end
end

function QuadTrees.eval(quad, boundingBox, func)
	-- split horizontally
	if boundingBox[1] < 0 then
		QuadTrees.eval(quad, { boundingBox[1] + QuadTrees.dimensions[1], boundingBox[2], QuadTrees.dimensions[1], boundingBox[4] }, func)
		QuadTrees.eval(quad, { 0, boundingBox[2], boundingBox[3], boundingBox[4] }, func )
		return
	elseif boundingBox[3] > QuadTrees.dimensions[1] then
		QuadTrees.eval(quad, { boundingBox[1], boundingBox[2], QuadTrees.dimensions[1], boundingBox[4] }, func )
		QuadTrees.eval(quad, { 0, boundingBox[2], boundingBox[3] - QuadTrees.dimensions[1], boundingBox[4] }, func)
		return
	end

	-- split vertically
	if boundingBox[2] < 0 then
		QuadTrees.eval(quad, { boundingBox[1], boundingBox[2] + QuadTrees.dimensions[2], boundingBox[3], QuadTrees.dimensions[2] }, func)
		QuadTrees.eval(quad, { boundingBox[1], 0, boundingBox[3], boundingBox[4] }, func )
		return
	elseif boundingBox[4] > QuadTrees.dimensions[2] then
		QuadTrees.eval(quad, { boundingBox[1], boundingBox[2], boundingBox[3], QuadTrees.dimensions[2] }, func )
		QuadTrees.eval(quad, { boundingBox[1], 0, boundingBox[3], boundingBox[4] - QuadTrees.dimensions[2] }, func)
		return
	end

	QuadTrees.evalQuad(quad, boundingBox, func)
end
-------------------------------------------------------

function QuadTrees.newQuad( coords )
	return { 
		box = coords, 
		size = {coords[3] - coords[1], coords[4] - coords[2] },
		children = {nil,nil,nil,nil}, 
		elements = {},
		parent = nil,
		childrenCount = 0,
		index = 0
	}
end

function QuadTrees.createChildAndInsert(quad, point, element)
  local ci = QuadTrees.indexFromPosition(point,quad)
  if not quad.children[ci] then
    local x = (ci - 1)%2
    local y = math.floor((ci-1)/2)
    local hs,vs = quad.size[1]*0.5, quad.size[2]*0.5
    quad.children[ci] = QuadTrees.newQuad( { quad.box[1] + hs*x, quad.box[2] + vs*y, quad.box[1]+hs*(x+1), quad.box[2]+vs*(y+1) } )
    quad.children[ci].parent = quad
    quad.children[ci].index = ci
    quad.childrenCount = quad.childrenCount + 1
    QuadTrees.totalCount = QuadTrees.totalCount + 1
  end
  QuadTrees.insert(quad.children[ci], point, element)
end

function QuadTrees.removeElement(quad, element)
	removeOne(quad.elements, element)
	if table.getn(quad.elements) == 0 then
		QuadTrees.purge(quad)			
	end
end

function QuadTrees.purge(quad)
	if quad.childrenCount == 0 and quad.parent then
		quad.parent.children[quad.index] = nil
		quad.parent.childrenCount = quad.parent.childrenCount - 1
		QuadTrees.totalCount = QuadTrees.totalCount - 1
		QuadTrees.purge(quad.parent)
	end
end

function QuadTrees.evalQuad(quad, boundingBox, func)
  if quad.childrenCount == 0 then
		for i,v in ipairs(quad.elements) do
			func(v)
		end
	else
		for i=1,4 do
			if quad.children[i] and QuadTrees.intersects(quad.children[i].box, boundingBox) then
				QuadTrees.evalQuad(quad.children[i], boundingBox, func)
			end
		end		
	end
end


function QuadTrees.pointInBox(point,quad)
	return point[1]>=quad.box[1] and point[1]<quad.box[3] and point[2]>=quad.box[2] and point[2]<quad.box[4]
end

function QuadTrees.indexFromPosition(point, quad)
	local p = {point[1] - quad.box[1], point[2] - quad.box[2]}
	local x,y = 1,0
	if p[1] >= quad.size[1]*0.5 then x = 2 end
	if p[2] >= quad.size[2]*0.5 then y = 2 end
	return x + y 
end


function QuadTrees.intersect(box1, box2)
	return box1[1] <= box2[2] and box1[2] >= box2[1] and box1[3] <= box2[4] and box1[4] >= box2[3]
end

function QuadTrees.intersects(box1, box2)
	return box1[1]<=box2[3] and box1[3]>box2[1] and box1[2]<=box2[4] and box1[4]>box2[2]
end

