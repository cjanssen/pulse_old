function math.sign( a )
	if a >= 0 then
		return 1
	else
		return -1
	end
end

function removeOne(t, element)
	for i,v in ipairs(t) do
		if v == element then
			table.remove(t,i)
			return
		end
	end
end

function distWrapSq(posA, posB)
	local dx = math.abs(posA[1] - posB[1])
	dx = math.min(dx, game.worldSize[1]-dx)
	local dy = math.abs(posA[2] - posB[2])
	dy = math.min(dy, game.worldSize[2]-dy)
	return dx*dx + dy*dy
end

function wrapCoord(x,y)
	return {x%game.worldSize[1], y%game.worldSize[2]}
end

function mapToScreena(x,y,layer)
	return mapToScreen2(x,y,layer)
end

function mapToScreen1( x, y, layer )
	local sw = Options.width / VideoModes.scale[1] * 0.5
	local sh = Options.height / VideoModes.scale[2] * 0.5

	local fac = 1
	local sc = game.screenCenter
	if layer == 2 then
		sc = game.layer2
		fac = 0.5
	elseif layer == 3 then
		sc = game.layer3
		fac = 0.25
	end
	local ww = game.worldSize[1] * fac
	local wh = game.worldSize[2] * fac

	local xret = x*fac - sc[1] + sw
	local yret = y*fac - sc[2] + sh

	if math.abs(xret + ww - sw) < math.abs(xret-sw) then
		xret = xret + ww
	end	
	if math.abs(xret - ww - sw) < math.abs(xret-sw) then
		xret = xret - ww
	end

	if math.abs(yret + wh - sh) < math.abs(yret-sh) then
		yret = yret + wh
	end	
	if math.abs(yret - wh - sh) < math.abs(yret-sh) then
		yret = yret - wh
	end

	return {xret, yret}
end

function mapToScreen( x, y, layer )
	return { 
		(x*game.layerFactors[layer] - game.layerCenters[layer][1] + game.screenWidth) % (game.worldSize[1]*game.layerFactors[layer]) - game.screenWidth*0.5,
		(y*game.layerFactors[layer] - game.layerCenters[layer][2] + game.screenHeight) % (game.worldSize[2]*game.layerFactors[layer]) - game.screenHeight*0.5
	}
end

function getAngle(posA, posB)
	return math.atan2(posB[2] - posA[2], posB[1] - posA[1]) * 180 / math.pi
end

function angleDiff(angleA, angleB)
	local diff = math.abs(angleB%360 - angleA%360)
	return math.min(diff, math.abs(360 - diff))
end

function increaseExponential(dt, var, amount)
	if var < 1 then
		var = 1 - (1 - var) * math.pow(amount, 60*dt)
		if var > 0.999 then
			var = 1
		end
	end
	return var
end

function decreaseExponential(dt, var, amount)
	if var > 0 then
		var = var * math.pow(amount, 60*dt)
		if var < 0.001 then
			var = 0
		end
	end
	return var
end

function linearApprox(dt, cur, dest, spd)
	local diff = dest - cur
	if math.abs(diff) > 0 then
		local inc = math.sign(diff) * spd * dt
		if math.abs(diff) < math.abs(inc) then
			inc = diff
		end
		cur = cur + inc
	end	
	return cur
end

function mixColors(colorA, colorB, fraction)
	return { colorA[1] * fraction + colorB[1]*(1-fraction),
		colorA[2] * fraction + colorB[2]*(1-fraction),
		colorA[3] * fraction + colorB[3]*(1-fraction) }
end

function hsv2rgb(h, s, v, a)
    if s <= 0 then return v,v,v,a end
    h, s, v = h/360*6, s/255, v/255
    local c = v*s
    local x = (1-math.abs((h%2)-1))*c
    local m,r,g,b = (v-c), 0,0,0
    if h < 1     then r,g,b = c,x,0
    elseif h < 2 then r,g,b = x,c,0
    elseif h < 3 then r,g,b = 0,c,x
    elseif h < 4 then r,g,b = 0,x,c
    elseif h < 5 then r,g,b = x,0,c
    else              r,g,b = c,0,x
    end 
	return (r+m)*255,(g+m)*255,(b+m)*255,a
end

function rgb2hsv(r, g, b, a)
	r,g,b = r/255, g/255, b/255
    local min = math.min(r,g,b)
	local max = math.max(r,g,b)

	local v = max
    local delta = max - min;
    if max > 0 then
        s = delta / max
	else 
		s = 0
		h = 0
		return 0,0,v*255,a
	end
	if r >= max then
		h = (g-b)/delta
	elseif g >= max then
		h = 2 + (b-r)/delta
	else
		h = 4 + (r-g)-delta
	end
    h = h * 60
    if h < 0 then
        h = h + 360
	end
    return h,s*255,v*255
end
