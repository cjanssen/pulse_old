Circles = {}


function Circles.init()
	Circles.list = {}

end

function Circles.newCircle(x,y,circleType)
	table.insert(Circles.list, {
		pos = {x,y},
		screenPos = {x,y},
		layer = 1,
		vel = {0,0},
		acc = {0,0},
		rad = 50,
		growthRate = 500,
		live = 8,
		cType = circleType,
		rotationSpeed = 0,
		angle = 0
	})
end

function Circles.forceLastCircleRadius(newRadius, newSpeed)
	Circles.list[table.getn(Circles.list)].rad = newRadius;
	Circles.list[table.getn(Circles.list)].growthRate = newSpeed;
end

function Circles.update( dt )
	local killlist = {}
	for i,v in ipairs(Circles.list) do
		if Circles.updateCircle( v, dt ) == false then
			table.insert(killlist,1,i)
		end
	end

	for i,v in ipairs(killlist) do
		table.remove(Circles.list,v)
	end
end

function Circles.draw()
	for i,v in ipairs(Circles.list) do
		Circles.drawCircle( v )
	end
end

function Circles.updateCircle( c, dt )
	c.rad = c.rad + c.growthRate * dt
	c.live = c.live - dt
	if c.live <= 0 then
		return false
	end
	updateMove( c, dt )
	mapCoords( c )
end

function Circles.drawCircle( c )
	local limit = 1000
	local alpha = ((limit - c.rad)/limit)
	if alpha > 1 then
		alpha = 1
	end
	if alpha < 0 then
		alpha = 0
	end
	alpha = 255 * alpha * alpha

	alpha = alpha * Titles.otherOpacity
	
	if c.cType==1 then
		love.graphics.setColor(128,255,128, alpha)
	elseif c.cType == 2 then
		love.graphics.setColor(128,255,128, alpha * 0.3)
	elseif c.cType == 3 then
		love.graphics.setColor(255,128,255, alpha)
	elseif c.cType == 4 then
		love.graphics.setColor(255,128,128, alpha)
	end
	love.graphics.circle("line", c.screenPos[1], c.screenPos[2], c.rad, 64)
	love.graphics.setColor(255,255,255,255)
end
