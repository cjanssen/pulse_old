
function include( filename )
	love.filesystem.load( filename )()
end


function loadDependencies()
	include("utils.lua")
	include("game.lua")
	include("player.lua")
	include("entity.lua")
	include("plankton.lua")
	include("circles.lua")
	include("friends.lua")
	include("sounds.lua")
	include("titles.lua")
	include("options.lua")
	include("videomodes.lua")
	include("quadtrees.lua")
end

function love.load()
	loadDependencies()
	game.testingValues()
	game.load()
end

function love.update(dt)
	game.update(dt)
end

function love.draw()
	game.draw()
end

function love.keypressed( key )
	game.keypressed(key)
end


function love.mousepressed(x, y , button)
	game.mousepressed(x,y,button)
end

function love.mousereleased(x, y , button)
	--game.mousereleased(x,y,button)
end

function love.quit()
	game.quitting()
end
