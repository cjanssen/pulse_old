Sounds = {}

function Sounds.init()
	Sounds.enabled = true
	if Sounds.enabled and not Sounds.loaded then
		Sounds.bgmusic = love.audio.newSource("snd/atmo.ogg","stream")
		Sounds.bgmusic:setLooping(true)		
		Sounds.heartbeat = love.audio.newSource("snd/thenudo-heartbeat01.ogg","stream")
		Sounds.friendNotes = {
			"snd/harp_c4.ogg",
			"snd/harp_d4.ogg", 
			"snd/harp_e4.ogg",
			"snd/harp_g4.ogg",
			"snd/harp_a4.ogg",

			"snd/harp_c5.ogg", 
			"snd/harp_d5.ogg",
			"snd/harp_e5.ogg",
			"snd/harp_g5.ogg",
			"snd/harp_a5.ogg"
		}

		Sounds.failNote = { 
			sound = love.audio.newSource("snd/harp_f2.ogg","stream"),
			naturalVol = 0.5 
		}
		Sounds.leaveNote = {
			sound = love.audio.newSource("snd/harp_d#3.ogg","stream"),
			naturalVol = 0.4
		}
		Sounds.angryNotes = {
			"snd/tuba_c2.ogg",
			"snd/tuba_d2.ogg",
			"snd/tuba_e2.ogg",
			"snd/tuba_g2.ogg",
			"snd/tuba_a2.ogg"
			}
		Sounds.beatNotes = {
			"snd/bass_c2.ogg",
			"snd/bass_d2.ogg",
			"snd/bass_e2.ogg",
			"snd/bass_g2.ogg",
			"snd/bass_a2.ogg"			 
		}

		Sounds.friendNotePool = {}
		Sounds.angryNotePool = {}
		Sounds.beatNotePool = {}
		Sounds.loaded = true

		Sounds.setVolumes()
	end
	Sounds.sfxMuted = false
end

function Sounds.setVolumes()
	if Options.musicVol > 1 then Options.musicVol = 1 end
	if Options.musicVol < 0 then Options.musicVol = 0 end
	if Options.sfxVol > 1 then Options.sfxVol = 1 end
	if Options.sfxVol < 0 then Options.sfxVol = 0 end
	Sounds.bgmusic:setVolume(Options.musicVol * 0.2)
	Sounds.heartbeat:setVolume(Options.sfxVol)
end

function Sounds.start()
	if Sounds.enabled then
		love.audio.setVolume(1)
		Sounds.playMusic()
		if Options.musicSwitch ~= 0 then
			love.audio.play(Sounds.bgmusic)
		end
	end
end

function Sounds.stop()
	Sounds.stopMusic()
end

function Sounds.tempMuteSfx()
	Sounds.sfxMuted = true
end

function Sounds.musicEnabled()
	return Sounds.enabled and Options.musicSwitch ~= 0
end

function Sounds.sfxEnabled()
	return Sounds.enabled and Options.sfxSwitch ~= 0 and not Sounds.sfxMuted
end

function Sounds.playMusic()
	if Sounds.musicEnabled() then
		love.audio.play(Sounds.bgmusic)
	end
end

function Sounds.stopMusic()
	if Sounds.enabled then
		love.audio.stop(Sounds.bgmusic)
	end
end

function Sounds.playHeartbeat()
	if Sounds.sfxEnabled() then
		Sounds.playSound(Sounds.heartbeat)
	end
end

function Sounds.playSound(sound)
	if not sound:isStopped() then
		sound:stop()
	end
	sound:play()
end

function Sounds.playNote(note)
	if Sounds.sfxEnabled() then
		note.sound:setVolume(Options.sfxVol * note.naturalVol * 0.6)
		Sounds.playSound(note.sound)
	end
end

function Sounds.playNoteVolume(note, modifVol)
	if Sounds.sfxEnabled() then
		note.sound:setVolume(Options.sfxVol * note.naturalVol * 0.6 * modifVol)
		Sounds.playSound(note.sound)
	end
end

function Sounds.getFromPool(pool, filenames)
	if table.getn(pool) == 0 then
		-- generate notepool
		local sortList = {}
		for i=1,table.getn(filenames) do
			table.insert(sortList,filenames[i])
		end
		while table.getn(sortList) > 0 do
			local index = math.random(table.getn(sortList))
			table.insert(pool, sortList[index])
			table.remove(sortList, index)
		end
	end

	local snd = love.audio.newSource(pool[1],"stream")
	table.remove(pool, 1)
	return snd
end

function Sounds.getFriendNote()
	return {
		sound = Sounds.getFromPool(Sounds.friendNotePool, Sounds.friendNotes),
		naturalVol = 0.3
	}
end

function Sounds.getAngryNote()
	return {
		sound = Sounds.getFromPool(Sounds.angryNotePool, Sounds.angryNotes),
		naturalVol = 1
	}
end

function Sounds.getBeatNote()
	return {
		sound = Sounds.getFromPool(Sounds.beatNotePool, Sounds.beatNotes),
		naturalVol = 1
	}
end


