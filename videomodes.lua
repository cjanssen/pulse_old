VideoModes = {}

function VideoModes.init()

	VideoModes.scale = {1, 1}
	VideoModes.zoom = {1, 1}
	game.zoom = 1
	VideoModes.list = love.graphics.getModes()
	
	local customList = {
  { width = 1920, height = 1440 },	
  { width = 1920, height = 1080 },
  { width = 1680, height = 1050 },
  { width = 1366, height = 768 },
  { width = 1280, height = 1024 },
  { width = 1280, height = 960 },
  { width = 1280, height = 720 },
  { width = 1024, height = 768 },
  { width = 800, height = 600 },
  { width = 640, height = 480 } }

	for i,v in ipairs(customList) do	
		table.insert(VideoModes.list, v)
	end
	table.sort(VideoModes.list, function(a, b) return a.width*a.height > b.width*b.height end)

	local n = table.getn(VideoModes.list)
	for i=1,n-1 do
		if VideoModes.list[n-i+1].width < 640 or VideoModes.list[n-i+1].width == VideoModes.list[n-i].width and VideoModes.list[n-i+1].height == VideoModes.list[n-i].height then
			table.remove(VideoModes.list, n-i+1)
		end
	end 

end
 
function VideoModes.getScreenDefaultOption(fullscreen)
	local fs = true
	if fullscreen == 0 then
		fs = false
	end

 	-- test from the list
	for i,v in ipairs(VideoModes.list) do
		if love.graphics.checkMode( v.width, v.height, fs ) then
			return v
		end
	end

	-- nothing!
	return nil
end

function VideoModes.reloadScreen()
	if Options.width == 0 then
		return
	end

	if game.screenWidth ~= Options.width or game.screenHeight ~= Options.height or game.fullscreen ~= Options.fullscreen then
		local w = love.graphics.getWidth()
		local h = love.graphics.getHeight()
		local f = game.fullscreen

		local fullscreen = false
		if Options.fullscreen ~= 0 then
			fullscreen = true
		end
		local success = love.graphics.checkMode( Options.width, Options.height, fullscreen ) and
						love.graphics.setMode( Options.width, Options.height, fullscreen )	
		if success then
			game.screenWidth = Options.width
			game.screenHeight = Options.height
			game.fullscreen = Options.fullscreen
		else
			Options.width = w
			Options.height = h
			Options.fullscreen = f
			fullscreen = false
			if Options.fullscreen ~= 0 then
				fullscreen = true
			end
			Options.save()
			love.graphics.setMode(w,h,fullscreen)
		end		
	end

	-- post-step
	VideoModes.zoom = {Options.width / 1024, Options.width / 1024}
	game.updateScaling(0, true)
end


